Function DPN_SLN(cost, start, life, period)
    'For full depreciation years
     If period > start And period <= (start + life) Then  'Calculate depreciation when the period is within applicable years
        DPN_SLN = cost * (1 / life)
    'for a partial year, if applicable
    ElseIf period > (start + Int(life)) And period <= (start + life + 1) Then
         DPN_SLN = cost * (1 / life) * (life - Int(life))
    Else
     DPN_SLN = 0 'if outside the depreciation range return zero
    End If
End Function

Function DPN_SYD(cost, start, life, period)
   'For full depreciation years
    If period > start And period <= (start + life) Then  'Calculate depreciation when the period is within applicable years
        DPN_SYD = cost * ((life + start - period + 1) / (life * (life + 1) / 2))  'Sum-of-years-digits formula
   'for a partial year, if applicable
    ElseIf period > (start + Int(life)) And period <= (start + life + 1) Then
      'for a partial year, SYD depreciation is the midpoint between the full year value and yearfrac * full year.
        fsyd = ((life + start - period + 1) / (life * (life + 1) / 2))
        psyd = fsyd * (life - Int(life))
        DPN_SYD = cost * ((fsyd + psyd) / 2)
      Else
        DPN_SYD = 0
    End If
End Function